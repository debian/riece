Source: riece
Section: net
Priority: optional
Maintainer: Hideki Yamane <henrich@debian.org>
Build-Depends: debhelper-compat (= 13),
Build-Depends-Indep: emacs | emacsen
Standards-Version: 4.5.0
Homepage: https://www.nongnu.org/riece/index.html.en
Vcs-Git: https://salsa.debian.org/debian/riece.git
Vcs-Browser: https://salsa.debian.org/debian/riece
Rules-Requires-Root: no

Package: riece
Architecture: all
Depends: ${misc:Depends}, debconf, emacsen-common (>= 2.0.8), emacs | emacsen
Replaces: riece-async, riece-google, riece-hangman, riece-kakasi, riece-lsdb, riece-ndcc, riece-rdcc, riece-xface
Conflicts: riece-async, riece-google, riece-hangman, riece-kakasi, riece-lsdb, riece-ndcc, riece-rdcc, riece-xface
Suggests: dictionaries-common, kakasi, ruby-gettext, lsdb
Description: IRC client for Emacs
 Riece is a user interface for IRC (Internet Relay Chat).  You should
 spell it with the first letter capitalized and pronounce it as /ri:s/.
 .
 The features of Riece are as follows:
 .
   * Several IRC servers may be used at the same time.
   * Many features built upon the extension mechanism called add-on.
     Currently 30 such add-ons are available.
   * Installation is easy.  Riece doesn't depend on other packages.
   * Setup is easy.  Automatically save/restore the configuration.
   * Step-by-step instructions are included.
   * Mostly compliant with the latest IRC client protocol (RFC 2812).
